<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('students.index');
});

Route::get('/students/grafik', [\App\Http\Controllers\StudentController::class, 'grafik'])->name('students.grafik');
Route::resource('/students', \App\Http\Controllers\StudentController::class);
