@extends('layouts.app')

@section('content')
    <div class="container py-5">
        <a href="{{route('students.create')}}" class="btn btn-primary mb-3">
            Tambah Mahasiswa
        </a>
        <div class="card">
            <div class="card-header d-flex align-items-center justify-content-between">
                <h5 class="card-title py-2 mb-0">
                    List Mahasiswa
                </h5>
            </div>
            <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Email</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($students as $student)
                    <tr>
                        <td>{{$student->nim}}</td>
                        <td>{{$student->name}}</td>
                        <td>{{$student->email}}</td>
                        <td>
                            <a href="{{route('students.show', $student)}}">Lihat Nilai</a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@endsection
