@extends('layouts.app')

@section('content')
    <div class="container py-5">

        @if(session('msg'))
            <div class="alert alert-success mb-5">
                {{session('msg')}}
            </div>
        @endif

        <div class="bg-light p-5 mb-5 text-center">
            <h2 class="mb-1">
                {{$student->name}}
            </h2>
            <span>{{$student->nim}} <span class="mx-1">|</span> {{$student->email}}</span>
        </div>

        <div class="mb-5 d-flex align-items-center justify-content-center">
            <label for="semester" class="me-2">Nilai Semester</label>
            <form action="" id="form-semester">
                <select name="semester" id="semester" class="form-control" style="min-width: 80px" onchange="document.getElementById('form-semester').submit()">
                    @foreach(range(1, 7) as $semester)
                    <option {{request('semester') == $semester ? 'selected' : ''}}>{{$semester}}</option>
                    @endforeach
                </select>
            </form>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="card-title py-2 mb-0">
                    List Nilai
                </h5>
            </div>
            <div class="card-body">
                <form action="{{route('students.update', $student)}}" method="POST">
                    <input type="hidden" value="{{request('semester')??1}}" name="semester">
                    @csrf
                    @method('PUT')
                    <table class="table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Aspek Nilai</th>
                            <th class="text-center">Nilai</th>
                            <th class="text-center">Grade</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($student->score_aspects as $i => $aspect)
                            <tr>
                                <td>{{$i+1}}</td>
                                <td>{{$aspect->name}}</td>
                                <td class="text-center">
                                    <input name="score_aspects[{{$aspect->id}}]" required type="number" min="0" max="100" value="{{$aspect->score}}" class="form-control text-center" style="margin: 0 auto; max-width: 150px">
                                </td>
                                <td class="text-center">{{$aspect->grade}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        @if($student->result)
                        <tr class="fw-bold">
                            <td></td>
                            <td>Result</td>
                            <td class="text-center">{{$student->result['average']}}</td>
                            <td class="text-center">{{$student->result['grade']}}</td>
                        </tr>
                        @endif
                        </tfoot>
                    </table>
                    <button class="btn btn-primary" type="submit">
                        Simpan Nilai
                    </button>
                </form>
            </div>
        </div>

    </div>
@endsection
