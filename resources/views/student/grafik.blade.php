@extends('layouts.app')

@section('content')
    <div class="container py-5">

        <div class="mb-5 d-flex align-items-center justify-content-center">
            <label for="semester" class="me-2">Pilih Semester</label>
            <form action="" id="form-semester">
                <select name="semester" id="semester" class="form-control" style="min-width: 80px" onchange="document.getElementById('form-semester').submit()">
                    @foreach(range(1, 7) as $semester)
                        <option {{request('semester') == $semester ? 'selected' : ''}}>{{$semester}}</option>
                    @endforeach
                </select>
            </form>
        </div>

        <div class="card">
            <div class="card-header">
                <h5 class="card-title py-2 mb-0">
                    Grafik Nilai
                </h5>
            </div>
            <div class="card-body">
                <canvas id="grade-chart"></canvas>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>
        const ctx = document.getElementById('grade-chart').getContext('2d');
        const gradeChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: {!! $grade->keys() !!},
                datasets: [{
                    label: 'Jumlah Mahasiswa',
                    data: {!! $grade->values() !!},
                    backgroundColor: [
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(255, 99, 132, 0.2)'
                    ],
                    borderColor: [
                        'rgba(75, 192, 192, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(255, 99, 132, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }
            }
        });
    </script>
@endsection
