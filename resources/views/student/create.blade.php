@extends('layouts.app')

@section('content')
    <div class="container py-5">
        <div class="bg-light p-5 mb-5 text-center">
            <h2 class="mb-0">
                Tambah Mahasiswa
            </h2>
        </div>

        @if($errors->all())
            <div class="alert alert-danger">
                <ul style="margin: 0">
                    @foreach($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div style="max-width: 400px; margin: 0 auto;">
            <form action="{{route('students.store')}}" method="POST">
                @csrf
                <div class="form-group mb-2">
                    <label for="nim">NIM</label>
                    <input type="text" id="nim" name="nim" class="form-control">
                </div>
                <div class="form-group mb-2">
                    <label for="name">Nama</label>
                    <input type="text" id="name" name="name" class="form-control">
                </div>
                <div class="form-group mb-3">
                    <label for="email">Email</label>
                    <input type="emaill" id="email" name="email" class="form-control">
                </div>
                <button type="submit" class="btn btn-primary">Tambahkan</button>
            </form>
        </div>
    </div>
@endsection
