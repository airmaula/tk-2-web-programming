<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\ScoreAspect;
use App\Models\Student;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // Score Aspects
        $score_aspects = [
            [
                'name' => 'Quiz',
            ],
            [
                'name' => 'Tugas',
            ],
            [
                'name' => 'Absensi',
            ],
            [
                'name' => 'Praktek',
            ],
            [
                'name' => 'UAS',
            ],
        ];
        foreach ($score_aspects as $score_aspect) {
            ScoreAspect::create($score_aspect);
        }

        // Students
        $students = [
            [
                'name' => 'Ahmad Irfan Maulana',
            ],
            [
                'name' => 'Noval Adianto',
            ],
            [
                'name' => 'Muhammad Ryan Alfauzaan',
            ],
            [
                'name' => 'Brian Gamaliel Sitepu',
            ],
            [
                'name' => 'Allysa Pebriliani',
            ],
        ];
        foreach ($students as $student) {
            $student = Student::create([
                'name' => $student['name'],
                'nim' => fake()->numberBetween(2400000000, 2499999999),
                'email' => fake()->email,
            ]);

            foreach (range(1, 7) as $semester) {
                foreach (ScoreAspect::all() as $aspect) {
                    $student->scores()->updateOrCreate([
                        'score_aspect_id' => $aspect->id,
                        'semester' => $semester
                    ], [
                        'score' => rand(75, 100),
                    ]);
                }
            }

        }
    }
}
