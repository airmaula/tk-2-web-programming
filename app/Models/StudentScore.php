<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentScore extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function getGradeAttribute()
    {
        if ($this->score <= 65) {
            return 'D';
        } else if ($this->score <= 75) {
            return 'C';
        } else if ($this->score <= 85) {
            return 'B';
        } else if ($this->score <= 100) {
            return 'A';
        }
        return null;
    }
}
