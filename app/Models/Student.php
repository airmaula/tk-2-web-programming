<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $guarded = [];

    public function getKey()
    {
        return "nim";
    }

    public function getRouteKeyName()
    {
        return "nim";
    }

    public function scores()
    {
        return $this->hasMany(StudentScore::class);
    }

    public function getScoreAspectsAttribute()
    {
        $that = $this;
        return ScoreAspect::all()->map(function($aspect) use ($that) {
            $semester = request('semester') ?? 1;
            $score = $that->scores()->where('score_aspect_id', $aspect->id)->where('semester', $semester)->first();

            $aspect->score = '-';
            $aspect->grade = '-';

            if ($score) {
                $aspect->score = $score->score;
                $aspect->grade = $score->grade;
            }

            return $aspect;
        });
    }

    public function getResultAttribute()
    {
        $semester = request('semester') ?? 1;
        if ($this->scores()->where('semester', $semester)->first()) {
            $average = $this->score_aspects->reduce(function($total, $score) {
                    $total += $score->score;
                    return $total;
                }, 0) / $this->score_aspects->count();

            $grade = '';

            if ($average <= 65) {
                $grade = 'D';
            } else if ($average <= 75) {
                $grade = 'C';
            } else if ($average <= 85) {
                $grade = 'B';
            } else if ($average <= 100) {
                $grade = 'A';
            }

            return [
                'average' => $average,
                'grade' => $grade,
            ];
        }
        return false;
    }
}
