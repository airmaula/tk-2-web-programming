<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScoreAspect extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $guarded = [];
}
