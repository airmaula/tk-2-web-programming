<?php

namespace App\Http\Controllers;

use App\Models\ScoreAspect;
use App\Models\Student;
use App\Models\StudentScore;
use Illuminate\Http\Request;

class StudentController extends Controller
{
    public function index()
    {
        // get all students sorted by name
        $students = Student::orderBy('name')->get();

        // return view
        return view('student.index', compact('students'));
    }

    public function show(Student $student)
    {
        // return view
        return view('student.show', compact('student'));
    }

    public function grafik(Student $student)
    {
        $students = Student::all()->filter(fn($student) => $student->result);

        $grade = collect([]);
        foreach (['A', 'B', 'C', 'D'] as $i) {
            $grade[$i] = $students->filter(fn($student) => $student->result['grade'] == $i)->count();
        }

        // return view
        return view('student.grafik', compact('student', 'grade'));
    }

    public function create()
    {
        // return view
        return view('student.create');
    }

    public function store(Request $request)
    {
        // validation
        $request->validate([
           'name' => ['required'],
           'email' => ['required', 'unique:students,email'],
           'nim' => ['required', 'unique:students,nim'],
        ]);

        // store
        $student = Student::create($request->only(['name', 'email', 'nim']));

        // response success
        return redirect()->route('students.show', $student)->with('msg', 'Mahasiswa berhasil ditambahkan');
    }

    public function update(Request $request, Student $student)
    {
        foreach ($request->score_aspects as $score_aspect_id => $score) {
            $student->scores()->updateOrCreate([
                'semester' => $request->semester,
                'score_aspect_id' => $score_aspect_id,
            ], [
                'score' => $score
            ]);
        }

        return redirect()->back()->with('msg', 'Nilai berhasil disimpan');
    }
}
